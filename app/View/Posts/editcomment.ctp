<table border="0" style="width:20%">
<tr>
<td><?php echo $this->Html->link('Home',array('controller' => 'posts', 'action' => 'index')); ?></td>
<td><?php echo $this->Html->link('Logout',array('controller' => 'users', 'action' => 'logout')); ?></td></tr></table>
<h1>Edit Comment</h1>
<?php
	echo $this->Form->create('Comment',array('enctype'=>'multipart/form-data','type'=>'file'));
	echo $this->Form->input('body');
	echo $this->Form->file('imageurl');
	echo $this->Form->input('id', array('type' => 'hidden'));
	echo $this->Form->input('foreign_id', array('type' => 'hidden'));
	echo $this->Form->end('Save Comment');
?>