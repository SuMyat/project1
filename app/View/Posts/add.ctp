<table border="0" style="width:20%">
<tr>
<td><?php echo $this->Html->link('Home',array('controller' => 'posts', 'action' => 'index')); ?></td>
<td><?php echo $this->Html->link('Logout',array('controller' => 'users', 'action' => 'logout')); ?></td></tr></table>
<h3>Add Topic</h3>
<?php
echo $this->Form->create('Post',array('enctype'=>'multipart/form-data','type'=>'file'));
echo $this->Form->input('title');
echo $this->Form->input('body', array('rows' => '3'));
echo $this->Form->file('imageurl');
echo $this->Form->end('Save Post');
?>