<table border="0" style="width:20%">
    <tr><td><?php echo $this->Html->link('Home',array('controller' => 'posts', 'action' => 'index')); ?></td>
        <td><?php echo $this->Html->link('Logout',array('controller' => 'users', 'action' => 'logout')); ?></td>
    </tr>
</table>
   
<h3>View Topic</h3>
<table>
<tr><td><h1><?php echo "Title: " . h($post['Post']['title']); ?></h1></td></tr>
<tr><td><p><small>Created: <?php echo $post['Post']['created']; ?></small></p></td></tr>
<tr><td><p><?php echo "Body: " . h($post['Post']['body']); ?></p></td></tr>
<tr><td><p><?php echo  $this->Html->image($post['Post']['imageurl'],array('width'=>'100px','heigh'=>'100px')); ?></p></td></tr>
</table>

<table>
    <tr>
        <th>Id</th>
        <th>Comment</th>       	
        <th>Created By</th>
        <th>Action</th>
    </tr>

	<?php foreach ($post['Comment'] as $key => $value): ?>	
	<tr>	
		<td><?php echo $value['id']; ?></td>	
		<td><?php echo $value['body']; ?></td>
		<td><?php echo $value['name']; ?></td>
        <td><?php echo $this->Html->image($value['imageurl'],array('width'=>'100px','high'=>'100px'));?></td>
		<td>
        <?php
            if(($_SESSION['Auth']['User']['username'] == $value['name']) || (AuthComponent::user('role') == 'admin'))
            {
                echo $this->Form->postLink('Delete',array('controller' => 'posts' , 'action' => 'deletecomment', $value['id'],$value['foreign_id']),array('confirm' => 'Are you sure?'));
                echo "\n";
                echo $this->Html->Link('Edit',array('controller' => 'posts' , 'action' => 'editcomment', $value['id'],$value['foreign_id'])); 
            }            
        ?>            
        </td>
		<?php echo '<br>'; ?>
	</tr>
	<?php endforeach; ?>
</table>

<?php echo $this->Form->create('Comment',array('enctype'=>'multipart/form-data','type'=>'file'),array('url'=>array('controller'=>'posts','action'=>'view',$post['Post']['id'])));?>
<fieldset>
    <legend><?php __('Add Comment');?></legend>
    <?php echo $this->Form->input('Comment.body');?>
    <?php echo $this->Form->file('Comment.imageurl');?>
</fieldset><?php echo $this->Form->end('Submit');?>
