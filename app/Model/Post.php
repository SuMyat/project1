<?php
	class Post extends AppModel
	{
		var $name = 'Post';
 		public $validate = array('title' => array('rule' => 'notBlank'),
 								'body' => array('rule' => 'notBlank'),
 								'imageurl' => array('rule' => 'notBlank')
 								);
 	
 		public function isOwnedBy($post, $user) 
 		{
    		return $this->field('id', array('id' => $post, 'user_id' => $user)) !== false;
		}
		
    	var $hasMany = array('Comment' => 
    				   array('className' => 'Comment','foreignKey' => 'foreign_id','conditions' => 
    				   array('Comment.class'=>'Post'),),);    	
	}
?>
