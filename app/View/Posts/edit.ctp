<table border="0" style="width:20%">
<tr>
<td><?php echo $this->Html->link('Home',array('controller' => 'posts', 'action' => 'index')); ?></td>
<td><?php echo $this->Html->link('Logout',array('controller' => 'users', 'action' => 'logout')); ?></td></tr></table>
<h1>Edit Topic</h1>
<?php
	echo $this->Form->create('Post');
	echo $this->Form->input('title');
	echo $this->Form->input('body', array('rows' => '3'));
	echo $this->Form->input('id', array('type' => 'hidden'));
	echo $this->Form->end('Save Post');
?>