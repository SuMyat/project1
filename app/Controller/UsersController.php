<?php

App::uses('AppController', 'Controller');

class UsersController extends AppController 
{

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow('add', 'logout', 'login');        
    }   
    public function login() 
    {
        if ($this->request->is('post')) 
    	{
        	if ($this->Auth->login()) 
        	{
           	 	$this->redirect($this->Auth->redirectUrl());
        	}
            else
            {
              $this->Flash->error(__('Invalid username or password, try again'));  
            }
        	
    	}
    }
	public function logout() 
	{
    	$this->redirect($this->Auth->logout());

        $this->Session->destroy('User'); 
        $this->Session->setFlash('You\'ve successfully logged out.');       
	}
    public function add() 
    {
        if ($this->request->is('post'))
        {
            $this->User->create();
            if ($this->User->save($this->request->data)) 
            {
                $this->Flash->success(__('The user has been saved'));
                return $this->redirect(array('controller' => 'users', 'action' => 'login'));
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
    }
}
