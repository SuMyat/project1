<table style="width:20%">
<tr>
<td><?php echo $this->Html->link('Home',array('controller' => 'posts', 'action' => 'index')); ?></td>
<td><?php echo $this->Html->link('Logout',array('controller' => 'users', 'action' => 'logout')); ?></td>
<td><?php echo $this->Html->link('Add Post',array('controller' => 'posts', 'action' => 'add')); ?></td></tr>
</table>

<?php echo $this->Form->create('Post' , array('url' => 'search'));?>
<h3>Topic Search</h3>
<table>
    <tr><td><?php echo $this->Form->input('Search.id'); ?></td>
        <td><?php echo $this->Form->input('Search.title'); ?></td>
        <td><?php echo $this->Form->submit('Search'); ?></td>
    </tr>
</table>    

<?php echo $this->Form->end(); ?> 
<?php $paginator = $this->Paginator; ?>   
<?php $paginator->options(array('url' => $this->passedArgs)); ?>
<table>
    <tr>
        <th>Id</th>
        <th>Title</th>
        <th>Action</th>
        <th>Created</th>
    </tr>
    <?php foreach ($posts as $post): ?>
    <tr>
        <td><?php echo $post['Post']['id']; ?></td>
        <td>
            <?php echo $this->Html->link($post['Post']['title'],array('controller' => 'posts', 'action' => 'view', $post['Post']['id'])); ?>
        </td>
        <td>        
            <?php 
                if(($_SESSION['Auth']['User']['id'] == $post['Post']['user_id']) || (AuthComponent::user('role') == 'admin'))
                {
                    echo $this->Form->postLink('Delete',array('action' => 'delete', $post['Post']['id']),array('confirm' => 'Are you sure?'));
                    echo "\n";
                    echo $this->Html->link('Edit', array('action' => 'edit', $post['Post']['id']));
                }
            ?>            
        </td>
        <td><?php echo $post['Post']['created']; ?></td>
    </tr>
    <?php endforeach; ?>
    <?php unset($post); ?>
</table>

<?php
    echo "<div class='paging'>";
    echo $paginator->first("First");        
    if($paginator->hasPrev())
    {
        echo $paginator->prev("Prev");
    }        
    echo $paginator->numbers(array('modulus' => 2));        
    if($paginator->hasNext())
    {
        echo $paginator->next("Next");
    }       
    echo $paginator->last("Last");     
    echo "</div>";
?>