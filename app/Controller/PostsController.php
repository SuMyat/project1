<?php
class PostsController extends AppController
{
	public $helpers = array('Html', 'Form', 'Flash', 'Session');
	public $components = array('Flash', 'Session');
    var $name = 'Posts';
    var $uses = array('Post','Comment');

     //for search
    public function search()
    {
        $url['action'] = 'index';
        foreach($this->data as $k => $v){           
            foreach($v as $kk => $vv){
                if ($vv) {
                    $url[$k.'.'.$kk]=$vv;
                }
            }
        }
        $this->redirect($url, null, true);      
    }	

	public function index()
	{
		$this->paginate = array('limit' => 5,'order' => array('id' => 'asc'));          	
    	$this->set('posts', $this->paginate('Post'));
        //for search
    	$title = array();
    	if(isset($this->passedArgs['Search.id'])) 
    	{  
            $id =  $this->passedArgs['Search.id'];            
    		$this->paginate = array ('conditions' => array('Post.id LIKE' => $id));
    		$posts = $this->paginate('Post');
    		$this->set('posts' , $posts);
			$this->request->data['Search']['id'] = $id;
			$title[] = __('ID',true).': '.$id;			
		}
		if(isset($this->passedArgs['Search.title'])) 
		{          
			$title =  $this->passedArgs['Search.title'];
            $this->paginate = array ('conditions' => array('Post.title LIKE' => '%' . $title . '%'));
            $posts = $this->paginate('Post');
            $this->set('posts' , $posts);
            $this->request->data['Search']['title'] = $title;
            $title1[] = __('Title',true).': '.$title;
        }
        $this->request->data=null;        
	}
    //for view
	public function view($id = null) 
	{
        if (!$id) 
        {
            throw new NotFoundException(__('Invalid post'));
        }

        $post = $this->Post->findById($id);
        if (!$post) 
        {
            throw new NotFoundException(__('Invalid post'));
        }
        //for comment view
        $this->set('post', $post);
        if (!$id) 
        {
           $this->Flash->error(__('Your Comment has not been saved.'));
            $this->redirect(array('action'=>'index'));
        }
        // save the comment
        if (!empty($this->request->data['Comment'])) 
        {
            $this->request->data['Comment']['class'] = 'Post'; 
            $this->request->data['Comment']['foreign_id'] = $id;
            $this->request->data['Comment']['name'] = $this->Auth->user('username'); 
            $this->Post->Comment->create();             
            //for photo comment
            if(!empty($this->request->data))
            {
                if(!empty($this->request->data['Comment']['imageurl']['name']))
                {
                    $file = $this->request->data['Comment']['imageurl']; 
                    $ext = substr(strtolower(strrchr($file['name'], '.')), 1); 
                    $arr_ext = array('jpg', 'jpeg', 'gif', 'png'); 
                    if(in_array($ext, $arr_ext))
                    {
                        move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/' . $file['name']);                    
                        $this->request->data['Comment']['imageurl'] = '/img/'.$file['name'];
                        pr($this->request->data);                        
                    }
                }
                else
                {
                    $this->request->data['Comment']['imageurl']=null;
                }     
            }
            if ($this->Post->Comment->save($this->request->data)) 
            {
                $this->Flash->success(__('Your Comment has been saved.'));            
                $this->redirect(array('action'=>'view', $id));
            }
            $this->Flash->warning(__('The Comment could not be saved. Please, try again.'));
        }
        // set the view variables
        $post = $this->Post->read(null, $id);
        $this->set(compact('post'));
    }
    //Post Add    
    public function add() 
    {       
        if ($this->request->is('post')) 
        {
            $this->request->data['Post']['user_id'] = $this->Auth->user('id');
            $this->Post->create();
           
            if(!empty($this->request->data))
            {
                if(!empty($this->request->data['Post']['imageurl']['name']))
                {
                    $file = $this->request->data['Post']['imageurl']; 
                    $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
                    $arr_ext = array('jpg', 'jpeg', 'gif', 'png'); 
                    if(in_array($ext, $arr_ext))
                    {
                        move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/' . $file['name']);                    
                        $this->request->data['Post']['imageurl'] = "/img/" . $file['name'];
                        pr($this->request->data);                        
                    }
                }                    
            }
            
            if ($this->Post->save($this->request->data)) 
            {
                $this->Flash->success(__('Your post has been saved.'));
                return $this->redirect(array('action' => 'index'));
            }
            else
            {
                $this->Flash->error(__('The data could not be saved. Please, Choose your image.'));
            }
        }
    }
    //Post edit
    public function edit($id = null) 
    {
        if (!$id) 
        {
            throw new NotFoundException(__('Invalid post'));
        }
        $post = $this->Post->findById($id);
        if (!$post) 
        {
            throw new NotFoundException(__('Invalid post'));
        }        
        if ($this->request->is(array('post', 'put'))) 
        {
            $this->Post->id = $id;
            if ($this->Post->save($this->request->data)) 
            {
                $this->Flash->success(__('Your post has been updated.'));
                return $this->redirect(array('action' => 'index'));
            } 
            $this->Flash->error(__('Unable to update your post.'));              
        }
        if (!$this->request->data) 
        {
            $this->request->data = $post;
        }
    }      
    
    //Post Delete
    public function delete($id) 
    {
        if ($this->request->is('get')) 
        {
            throw new MethodNotAllowedException();
        }       
        if ($this->Post->delete($id)) 
        {
            $this->Flash->success(
                __('The post with id: %s has been deleted.', h($id))
                );
        }        
        return $this->redirect(array('action' => 'index'));
    }
    public function isAuthorized($user) 
    {
        // All registered users can add posts
        if ($this->action === 'add') 
        {
            return true;
        }
        // The owner of a post can edit and delete it
        if (in_array($this->request->action, array('edit', 'delete'))) 
        {
            $postId = (int) $this->request->params['pass'][0];
            if ($this->Post->isOwnedBy($postId, $user['id'])) 
            {
                return true;
            }
        }
        return parent::isAuthorized($user);
    }

    //Delete Comment
    public function deletecomment($id,$foreign_id) 
    {
        if ($this->request->is('get')) 
        {
            throw new MethodNotAllowedException();
        }

        if ($this->Comment->delete($id))         
        {   
            $this->Comment->id = $id;
            $this->Comment->delete($this->request->data('Comment.imageurl'));        
            return $this->redirect(array('controller' => 'posts', 'action' => 'view',$foreign_id));
        }
    } 
    //Edit Comment    
    public function editcomment($id = null,$foreign_id = null) 
    {
        $this->Comment->id = $id;
        $this->Post->id = $foreign_id;       
        if(!empty($this->request->data['Comment']['imageurl']['name']))
        {
            $file = $this->request->data['Comment']['imageurl']; 
            $ext = substr(strtolower(strrchr($file['name'], '.')), 1); 
            $arr_ext = array('jpg', 'jpeg', 'gif', 'png'); 
            {
                move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/' . $file['name']);                    
                $this->request->data['Comment']['imageurl'] = '/img/'.$file['name'];
                pr($this->request->data);                        
            }            
        }
            
        if ($this->request->is('get')) 
        {
            $this->request->data = $this->Comment->read();
        }
        else
        {
            if ($this->Comment->save($this->request->data)) 
            {
                
               $this->redirect(array('controller' => 'posts','action' => 'view' , $foreign_id));
            }
            else
            {
                $this->Flash->error(__('Unable to update your post.'));
            } 
        }                         
    }
}   



?>